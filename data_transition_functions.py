import os
import pickle

from data import heroes_saves_folder_path, HEADERSIZE, file_name, package_size


def translate_save_to_bytes():
    with open(os.path.join(heroes_saves_folder_path, file_name), 'rb') as file:
        msg = file.read()
    msg = pickle.dumps(msg)
    return bytes(f'{len(msg):<{HEADERSIZE}}', 'utf-8') + msg


def save_bytes_as_save_file(full_message):
    d = pickle.loads(full_message[HEADERSIZE:])
    with open(file_name, 'wb') as file:
        file.write(d)


def receive_data_from_socket(socket):
    full_message = b''
    new_message = True
    while True:
        msg = socket.recv(package_size)
        if new_message:
            if msg[:HEADERSIZE].decode('utf-8') == "Receiving data"[:HEADERSIZE]:
                return False
        if new_message:
            print("Receiving data")
            print(f"New message length: {msg[:HEADERSIZE].decode('utf-8')}")
            message_length = int(msg[:HEADERSIZE].decode('utf-8'))
            new_message = False
        full_message += msg
        if len(full_message) - HEADERSIZE == message_length:
            save_bytes_as_save_file(full_message)
            print("Received data")
            return True
