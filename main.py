import time

import keyboard
import os


if __name__ == '__main__':
    while True:
        if keyboard.is_pressed('f5'):
            time.sleep(2)
            os.system('python client_send_data.py')
        if keyboard.is_pressed('f6'):
            os.system('python client_receive_data.py')
