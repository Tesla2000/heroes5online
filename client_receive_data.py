import socket

from data import port, host_address
from data_transition_functions import receive_data_from_socket

if __name__ == '__main__':
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    host = socket.gethostbyname(host_address)
    s.connect((host_address, port))
    s.send(bytes("Receiving data", "utf-8"))
    receive_data_from_socket(s)
    s.close()
