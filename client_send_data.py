import socket

from data import port, host_address
from data_transition_functions import translate_save_to_bytes

if __name__ == '__main__':
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    host = socket.gethostbyname(host_address)
    s.connect((host_address, port))
    s.send(translate_save_to_bytes())
    s.close()
