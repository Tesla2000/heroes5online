import socket

from data import port, host_address
from data_transition_functions import receive_data_from_socket, translate_save_to_bytes

if __name__ == '__main__':
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    host = socket.gethostbyname(host_address)
    s.bind((host, port))
    s.listen(8)
    while True:
        client_socket, address = s.accept()
        print(f"Connection from {address} has been established!")
        full_message = b''
        new_message = True
        if not receive_data_from_socket(client_socket):
            print("Sending data to client")
            client_socket.send(translate_save_to_bytes())
            print("Sent data to client")
